#!/usr/bin/env python
import rospy
import time
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_mfrc522
from finch_bot.msg import finch_aux_cmd

class finch_card_util(object):

    def mfrc_cb(self, msg):
	ac = finch_aux_cmd()

	if(msg.long_id != "2251972559"):
		ac.led[0] = 255;
		ac.led[1] = 255;
		ac.led[2] = 255;
	else:
		ac.led[0] = 0;
		ac.led[1] = 0;
		ac.led[2] = 0;
	
	self.pub.publish(ac);


    def __init__(self, *args, **kwds):

        rospy.init_node('finch_card_util', anonymous=True)

        self.pub = rospy.Publisher('finch_aux_cmd', finch_aux_cmd, queue_size=10)
        rospy.Subscriber('finch_mfrc', finch_mfrc522, self.mfrc_cb)

        rospy.spin()

    def __del__(self):
        pass

if __name__ == '__main__':
    fd = finch_card_util()
