source ~/catkin_foo/devel/setup.bash
export ROS_MASTER_URI=http://192.168.1."$1":11311
export ROS_HOSTNAME="$(ifconfig wlan0 | awk '/t addr:/{gsub(/.*:/,"",$2);print$2}')"
rosparam set line_gain "$2"

rosrun finch_bot snap_server.py
